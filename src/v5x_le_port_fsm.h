#pragma once

#include <stdint.h>

struct v5x_user_port;
enum v5x_mph_prim;
struct osmo_fsm_inst;

void v5x_port_mph_snd(struct v5x_user_port *v5up, enum v5x_mph_prim prim);

struct osmo_fsm_inst *v5x_le_port_isdn_create(struct v5x_user_port *v5up, uint16_t nr);
struct osmo_fsm_inst *v5x_le_port_pstn_create(struct v5x_user_port *v5up, uint16_t nr);
bool v5x_le_port_isdn_is_operational(struct osmo_fsm_inst *fi);
void v5x_le_port_isdn_block(struct osmo_fsm_inst *fi);
void v5x_le_port_isdn_unblock(struct osmo_fsm_inst *fi);
bool v5x_le_port_pstn_is_operational(struct osmo_fsm_inst *fi);
void v5x_le_port_pstn_block(struct osmo_fsm_inst *fi);
void v5x_le_port_pstn_unblock(struct osmo_fsm_inst *fi);
const char *v5x_le_port_isdn_state_name(struct osmo_fsm_inst *fi);
const char *v5x_le_port_pstn_state_name(struct osmo_fsm_inst *fi);

void v5x_le_port_isdn_fe_rcv(struct v5x_user_port *v5up, uint8_t cfe, uint8_t perf_grading);
void v5x_le_port_pstn_fe_rcv(struct v5x_user_port *v5up, uint8_t cfe);

void v5x_le_port_init(void);
