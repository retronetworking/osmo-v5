
enum at_fail {
	AT_FAIL_NONE = 0,
	AT_FAIL_MIN_LEVEL,
	AT_FAIL_MAX_NOISE,
	AT_FAIL_MAX_FREQUENCY,
};

enum at_state {
	AT_STATE_NONE = 0,
	AT_STATE_DETECT,
	AT_STATE_TONE,
	AT_STATE_PAUSE,
};

struct answer_tone {
	float sine;
	float cosine;
	float coeff;
	float *buffer;
	int buffer_size;
	int buffer_pos;
	float last_phase;
	enum at_state state;
	float tone_duration;
	float frequency_sum;
	int count;
	float last1_valid_phase;
	float last2_valid_phase;
	float last_valid_frequency;
};

int answertone_init(struct answer_tone *at, int samplerate);
void answertone_reset(struct answer_tone *at);
void answertone_exit(struct answer_tone *at);
enum at_fail answertone_check(struct answer_tone *at, float *phase_p, float *frequency_p);
int answertone_process(struct answer_tone *at, int16_t *data, int length, bool phase_reversal);
