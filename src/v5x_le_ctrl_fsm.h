#pragma once

#include <stdint.h>

struct v5x_ctrl_proto;
struct v5x_interface;
struct v5x_user_port;
enum v5x_ctrl_type;
enum v5x_ctrl_func_id;
enum v5x_ctrl_func_el;
enum v52_link_ctrl_func;
struct tlv_parsed;

void v5x_le_ctrl_init(void);
struct v5x_ctrl_proto *v5x_le_ctrl_create(enum v5x_ctrl_type type, void *ctx, void *priv, uint16_t nr);
void v5x_le_ctrl_destroy(struct v5x_ctrl_proto *ctrl);
int v5x_le_ctrl_dl_rcv(struct v5x_interface *v5if, uint16_t l3_addr, bool is_isdn, uint8_t msg_type,
		const struct tlv_parsed *tp);
int v5x_le_ctrl_common_snd(struct v5x_interface *v5if, enum v5x_ctrl_func_id cfi,
		uint8_t *rej_cause, uint8_t *variant, uint32_t *interface_id, uint8_t *ie_0x24);
int v5x_le_ctrl_port_snd(struct v5x_user_port *v5up, enum v5x_ctrl_func_el cfe);
int v52_le_ctrl_link_snd(struct v5x_link *v5l, enum v52_link_ctrl_func lcf);
void v5x_le_ctrl_start(struct v5x_ctrl_proto *ctrl);
void v5x_le_ctrl_stop(struct v5x_ctrl_proto *ctrl);
bool v5x_le_ctrl_is_in_service(struct v5x_ctrl_proto *ctrl);
