#pragma once

#include <stdint.h>

struct v5x_pstn_proto;
struct v5x_user_port;
struct v5x_l1_proto;
struct v5x_user_port;
enum v5x_fe_prim;
enum v5x_mgmt_prim;
struct msgb;
struct tlv_parsed;

struct v5x_pstn_proto *v5x_le_pstn_create(struct v5x_user_port *v5up, uint16_t nr);
void v5x_le_pstn_destroy(struct v5x_pstn_proto *pstn);
const char *v5x_le_pstn_state_name(struct v5x_pstn_proto *pstn);
void v5x_le_pstn_init(void);
void v5x_le_pstn_fe_snd(struct v5x_user_port *v5up, enum v5x_fe_prim prim, struct msgb *msg);
void v5x_le_pstn_mdu_snd(struct v5x_user_port *v5up, enum v5x_mgmt_prim prim);
int v5x_le_pstn_dl_rcv(struct v5x_user_port *v5up, uint8_t msg_type, const struct tlv_parsed *tp);
