#pragma once

extern int16_t g711_alaw_flipped_to_linear[256];
extern int16_t g711_ulaw_flipped_to_linear[256];
extern uint8_t g711_linear_to_alaw_flipped[65536];
extern uint8_t g711_linear_to_ulaw_flipped[65536];

void g711_init(void);
